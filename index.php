<!doctype html>
<html lang="ru">
<head>


    <?php require_once 'db_connect.php'; ?>


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Отзывы</title>
</head>
<body>
<!---->
<?php ////маленткая проверка на добавление данных?>
<?php //if(isset($result)) {
//    if ($result) { ?>
<!--        <p>Данные из формы добавлены успешно!</p>-->
<!--    --><?php //} else { ?>
<!--        <p>Ошибка добавления данных!</p>-->
<!---->
<!--    --><?php //}
//}
//?>


<?php
//Вывод записей
while(($row=$result->fetch_assoc())!=false){?>

<div class="container py-3 my-3">
    <div class="row"  >
        <div class="col-3 ">
            <h4 class="my-1 text-center"><?php echo $row['author']?></h4>
            <div class="img-thumbnail bg-light align-middle text-center mx-auto" style="width: 200px; height: 200px; margin: 10px 10px 30px 10px;"><p style="margin-top: 80px;">200x200</p></div>
        </div>
        <div class="col-9">
            <p class="text-right mr-3"><?php echo $row['date']?></p>

            <div class="bg-light" style="margin: 10px 10px 0px 10px;">
                <?php echo $row['message']?>
            </div>
        </div>
    </div>
<?php
}
?>

    <div class="row">
<div class="col-12">






    <form name="review" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
        <div class="form-group">
            <label for="author">Ваше имя:</label>
            <input type="text" class="form-control" name="author" id="author" placeholder="Введите ваше имя...">
        </div>

        <div class="form-group">
            <label for="message">Введите ваше сообщение:</label>
            <textarea class="form-control" name="message" id="message" rows="5"></textarea>
        </div>
        <button type="submit" name="review" class="btn btn-primary" value="send">Отправить</button>
    </form>



</div>



    </div>













</div>







<nav aria-label="Reviews pages">
    <ul class="pagination justify-content-center fixed-bottom">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>


        <?php
        for ($i=1; $i<=$pagesCount; $i++)//постраничная навигация

        {

            if ($i==$requestedPage)
                echo '<li class="page-item active" aria-current="page">
      <a class="page-link" href="#">'.$i.'<span class="sr-only">(current)</span></a>
    </li>';

            else

                echo '<li class="page-item"><a class="page-link" href="' . $_SERVER['SCRIPT_NAME'] .
                    '?page='.$i.'">'.$i.'</a></li>';

        }

        ?>



        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>










<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
